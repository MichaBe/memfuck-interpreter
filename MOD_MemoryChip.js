(function() {
    var app=angular.module("MemoryChip",[]);

    app.directive("chipElement", function() {
       return {
           restrict: 'E',
           templateUrl: 'chipElement.html',
           controller: ["Memory", function(Memory){
                this.mem = {cells : Memory.memory};
                this.hasActiveInput = false;
                this.aX = 0;
                this.aY = 0;
                this.NumAt = function(n,m) {
                    return Memory.memory[m][n];
                }
                this.isActive = function(n,m) {
                    return Memory.isActive(n,m);
                }
                this.addToReference = function(n,m) {
                    Memory.addToReference(n,m);
                };
                this.getC = function(n,m) {
                    return Memory.getChar(n,m);
                };
                this.activateInput = function(n,m) {
                    if(!this.hasActiveInput) {
                        this.hasActiveInput = true;
                        this.aX = n;
                        this.aY = m;
                    }
                };
                this.isActiveInput = function(n,m) {
                    if(this.hasActiveInput && n == this.aX && m == this.aY) {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                this.newInput = function(cInput, x,y, iInputSwitch) {
                    var saveInput = -1;
                    var useInput = false;

                    if(iInputSwitch == 0) { //Zahlen
                        if(isNaN(Number(cInput)) == false && cInput != undefined) {
                            saveInput = Number(cInput);
                        }
                    }
                    else { //Buchstaben
                        if(cInput !== undefined) {
                            saveInput = cInput.charCodeAt(0);//TODO leerzeichen wird nicht registriert...
                        }
                    }
                    if(0 <= saveInput && saveInput <= 255) {
                        useInput = true;
                    }

                    //input speichern
                    if(useInput) {
                        this.hasActiveInput = false;
                        Memory.memory[y][x] = saveInput;
                    }
                }
           }],
           controllerAs: 'memoryCtrl'
       };
    });
})();
(function(){
    var app = angular.module("MemFuckII", ["TuringTape", "MemoryChip", "StackSave"]);
    
    var getC = function(ASCII) {
        return String.fromCharCode(ASCII);
    }

    var LogDom = null;
    var isValidLogDom = false;
    
    app.controller("tinyScroller", function() {//For scrolling the Debuging-Output to the bottem with every new message
        if(isValidLogDom) { LogDom.scrollTop = LogDom.scrollHeight; }
    });
    
    app.controller("panelController", function() {
        this.active = 1;

        this.setActive = function(n) {
            this.active = n;
        };
        this.isActive = function(n) {
            return (this.active == n);
        };
    });
    app.directive("manPage", function() {
        return {
            restrict: 'E',
            templateUrl: 'manPage.html'
        };
    });
    app.filter('ToLen', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });
    
    app.service("MyConsole", ["Memory","Tape",function(Memory, Tape) {
        this.debugOutputs = Array(0);//Zweidimensionales Array, Erstes Feld: E/W/M, Zweites Feld: Nachricht
        this.debugOutputs.push(['M',"Message: MyConsole Interpreter output initialized"]);
        

        this.showErrors = true;
        this.showWarnings = true;
        this.showMessages = true;

        this.debugOut = function(level, message) {
            this.debugOutputs.push([level,message]);
        };
        this.toggleErrors = function() {
            this.showErrors = !this.showErrors;
        };
        this.toggleWarnings = function() {
            this.showWarnings = !this.showWarnings;
        };
        this.toggleMessages = function() {
            this.showMessages = !this.showMessages;
        };
        this.clearDebugOut = function() {
            this.debugOutputs.length = 0;
        };

        this.ConsoleOutput = String();
        this.consoleOut = function(character) {
            this.ConsoleOutput += character;
        };
        
        this.consoleIn = function(iInputSwitch, inputX) {//int 0, char 1
            if(userInputIsActive) {
                var saveInput = -1;
                var useInput = false;

                if(iInputSwitch == 0) { //Zahlen
                    if(isNaN(Number(inputX)) == false && inputX != undefined) {
                        saveInput = Number(inputX);
                    }
                }
                else { //Buchstaben
                    if(inputX !== undefined) {
                        saveInput = inputX.charCodeAt(0);//TODO leerzeichen wird nicht registriert...
                    }
                }
                if(0 <= saveInput && saveInput <= 255) {
                    useInput = true;
                }
                //input speichern
                if(useInput) {
                    userInputIsActive = false;
                    Tape.setValue(saveInput);
                    Memory.addToReference(globalX, globalY);
                }
                else {
                    this.debugOutputs.push(["E", "Error: Invalid Userinput (Only one letter / numbers between 0 and 255 are allowed). Please repeat!"]);
                }
            }
            else {
                this.debugOutputs.push(['W', 'Warning: No Userinput expected']);
            }
        };

    }]);

    app.directive("controlRow", function(){
        return {
            restrict: 'E',
            templateUrl: 'controlRow.html',
            controller: ["Interpreter", "MyConsole", "Memory", "$interval", function(Interpreter, MyConsole, Memory, $interval){
                    this.Out = {Out:MyConsole.debugOutputs};

                    this.consoleInput = function(n, x) {
                        MyConsole.consoleIn(n, x);
                    };

                    LogDom = document.getElementById("InterpreterOutput");
                    isValidLogDom = true;
                    this.writeLog = function(level, message) {
                        MyConsole.debugOut(level, message);
                    };
                    this.clearDO = function() {
                        MyConsole.clearDebugOut();
                    };
                    this.isVisible = function(level) {
                        var returner = false;
                        switch(level) {
                            case 'E':
                                returner = MyConsole.showErrors;
                                break;
                            case 'W':
                                returner = MyConsole.showWarnings;
                                break;
                            case 'M':
                                returner = MyConsole.showMessages;
                        }
                        return returner;
                    };
                    this.toggleLevel = function(level) {
                        switch(level) {
                            case 'E':
                                MyConsole.toggleErrors();
                                break;
                            case 'W':
                                MyConsole.toggleWarnings();
                                break;
                            case 'M':
                                MyConsole.toggleMessages();
                                break;
                        }
                    };

                    this.cOut = function(){return MyConsole.ConsoleOutput};
                    
                    this.STEP = function() {
                        if(!angular.isDefined(this.promise)) {
                            Interpreter.Step();
                        }
                    };
                    this.loadExample1 = function() {
                        this.CLEAR();
                        Memory.memory[0][0]=Memory.memory[5][0]=118;
                        Memory.memory[1][0]=Memory.memory[4][0]=Memory.memory[6][2]=44; 
                        Memory.memory[1][8]=Memory.memory[1][10]=Memory.memory[2][6]=Memory.memory[2][8]=Memory.memory[2][10]=Memory.memory[5][12]=Memory.memory[6][15]=Memory.memory[7][12]=Memory.memory[10][6]=Memory.memory[10][8]=Memory.memory[10][10]=Memory.memory[11][8]=Memory.memory[11][10]=93; 
                        Memory.memory[2][0]=94; 
                        Memory.memory[2][5]=Memory.memory[6][0]=Memory.memory[6][3]=Memory.memory[10][5]=62; 
                        Memory.memory[3][0]=Memory.memory[6][1]=Memory.memory[6][4]=125; 
                        Memory.memory[3][5]=Memory.memory[6][11]=123; 
                        Memory.memory[3][8]=Memory.memory[3][10]=Memory.memory[9][8]=Memory.memory[9][10]=63;
                        Memory.memory[4][5]=107;
                        Memory.memory[4][8]=99;
                        Memory.memory[4][10]=117;
                        Memory.memory[5][5]=Memory.memory[5][8]=Memory.memory[5][10]=Memory.memory[6][13]=Memory.memory[7][5]=Memory.memory[7][8]=Memory.memory[7][10]=34; 
                        Memory.memory[6][5]=Memory.memory[6][10]=91; 
                        Memory.memory[6][6]=Memory.memory[6][7]=Memory.memory[6][9]=Memory.memory[9][5]=42;
                        Memory.memory[6][8]=91;
                        Memory.memory[6][12]=33;
                        Memory.memory[6][14]=77;
                        Memory.memory[8][5]=70;
                        Memory.memory[8][8]=109;
                        Memory.memory[8][10]=101;
                        this.writeLog("M","Message: loaded example 1");
                        this.writeLog("M","Take a closer look, how the direction of the execution");
                        this.writeLog("M","is kept the same by the loop, so every branch is executed once.");
                        this.writeLog("M","Also the '!' itself does not move the execution to the next step.");
                        this.writeLog("M","This has to be done by the command on the tape.");
                    };

                    this.loadExample2 = function() {
                        this.CLEAR();
                        Memory.memory[0][0]=44;
                        Memory.memory[0][1]=10;
                        Memory.memory[0][2]=91;
                        Memory.memory[0][3]=35;
                        Memory.memory[0][5]=94;
                        Memory.memory[15][5]=42;
                        Memory.memory[14][5]=Memory.memory[8][7]=60;
                        Memory.memory[14][4]=118;
                        Memory.memory[15][4]=61;
                        Memory.memory[1][4]=Memory.memory[10][9]=62;
                        Memory.memory[1][5]=45;
                        Memory.memory[1][6]=93;
                        Memory.memory[1][7]=Memory.memory[2][11]=Memory.memory[4][11]=Memory.memory[6][13]=Memory.memory[8][10]=Memory.memory[10][13]=63;
                        Memory.memory[1][8]=Memory.memory[2][12]=Memory.memory[4][12]=Memory.memory[6][14]=Memory.memory[8][9]=Memory.memory[10][14]=124;
                        Memory.memory[2][1]=Memory.memory[2][3]=Memory.memory[2][5]=Memory.memory[2][7]=Memory.memory[2][9]=Memory.memory[4][3]=Memory.memory[4][5]=Memory.memory[4][7]=Memory.memory[4][9]=Memory.memory[6][5]=Memory.memory[6][7]=Memory.memory[6][9]=Memory.memory[6][11]=Memory.memory[8][6]=Memory.memory[8][4]=Memory.memory[8][2]=Memory.memory[8][0]=Memory.memory[8][14]=Memory.memory[8][12]=34;
                        Memory.memory[2][2]=68;
                        Memory.memory[2][4]=Memory.memory[4][4]=Memory.memory[6][8]=105;
                        Memory.memory[2][6]=Memory.memory[6][6]=Memory.memory[8][3]=101;
                        Memory.memory[2][8]=Memory.memory[4][6]=Memory.memory[8][1]=115;
                        Memory.memory[2][10]=Memory.memory[4][10]=Memory.memory[6][12]=Memory.memory[8][11]=32;
                        Memory.memory[4][8]=Memory.memory[8][15]=116;
                        Memory.memory[6][10]=110;
                        Memory.memory[8][5]=84;
                        Memory.memory[8][13]=33;
                        Memory.memory[10][10]=125;
                        Memory.memory[10][11]=43;
                        Memory.memory[10][12]=123;
                        this.writeLog("M","Message: loaded example 2");
                        this.writeLog("M","Using the stack to jump to absolute postions.");
                    }

                    this.promise = undefined;
                    this.runner = function() {
                        if(Interpreter.running && !userInputIsActive) {
                            Interpreter.Step();
                        }
                        else {
                            $interval.cancel(this.promise);
                            this.promise = undefined;
                        }
                    };
                    this.RUN = function() {
                        if(angular.isDefined(this.promise)) { return; }
                        else {
                            this.promise = $interval(this.runner, 25);
                        }
                        
                    };
                    this.STOP = function() {
                        $interval.cancel(this.promise);
                        this.promise = undefined;
                    };
                    this.RESET = function() {
                        MyConsole.ConsoleOutput = "";
                        $interval.cancel(this.promise);
                        this.promise = undefined;
                        Interpreter.Reset();
                    };
                    this.CLEAR = function() {
                        MyConsole.ConsoleOutput = "";
                        $interval.cancel(this.promise);
                        this.promise = undefined;
                        Interpreter.Init();
                        MyConsole.clearDebugOut();
                    };
            }],
            controllerAs: "rowCtrl"
        };
    });

    app.service("Tape", function() {
       var tapeSize = 16;
       var byteDepth = 256;
       this.tape = Array(tapeSize);
       this.active = 0;
       for(var i = 0; i < tapeSize; i++)
        this.tape[i] = 0;
       
       this.Reset = function() {
           this.tape = Array(tapeSize);
           this.active = 0;
           for(var i = 0; i < tapeSize; i++)
                this.tape[i] = 0;
       };
       this.getChar = function(n) {
           return getC(this.tape[n]);
       }

       this.isActive = function(index) {
           return this.active === index;
       };
       this.addToReference = function(n) {
           this.active +=n;
           while(this.active < 0)
                this.active+=tapeSize;
           this.active%=tapeSize;
       };
       this.addToValue = function(n) {
         this.tape[this.active]+=n;
         while(this.tape[this.active] < byteDepth)
            this.tape[this.active]+=byteDepth;
         this.tape[this.active] %= byteDepth;
       };
       this.setValue = function(n) {
           this.tape[this.active]=n;
           while(this.tape[this.active] < byteDepth)
            this.tape[this.active]+=byteDepth;
           this.tape[this.active] %= byteDepth;
       };
       this.getValue = function(index) {
           return this.tape[index%16];
       };
    });

    app.service("Memory", function(){
       var memSize = 16;
       var byteDepth = 256;
       this.memory = Array(memSize);
       this.x = 0;
       this.y = 0;
       this.Reset = function() {
           this.memory = Array(memSize);
           this.x = 0;
           this.y = 0;
           for(var i = 0; i < memSize; i++) {
                this.memory[i] = Array(memSize);
                for(var j = 0; j < memSize; j++) {
                    this.memory[i][j] = 0;
                }
            }
       };
       for(var i = 0; i < memSize; i++) {
            this.memory[i] = Array(memSize);
            for(var j = 0; j < memSize; j++) {
                this.memory[i][j] = 0;
            }
       }
       this.getChar = function(n,m) {
           return getC(this.memory[m][n]);
       }
       this.addToReference = function(n,m) {
           this.x += n;
           this.y += m;
           while(this.x < 0) {
                this.x += memSize;
           }
           while(this.y < 0) {
               this.y += memSize;
           }
           this.x %= memSize;
           this.y %= memSize;
       };
       this.setReference = function(n,m) {
           this.x = n;
           this.y = m;
       };
       this.setValue = function(n) {
           this.memory[this.y][this.x] = n%byteDepth;
       };
       this.getValue = function(){
           return this.memory[this.y][this.x];
       }
       this.isActive = function(n,m){
            return (this.x === n && this.y ===m);  
       };
    });

    app.service("Stack", function() {
        this.stack = Array(16);
        this.active = -1;
        this.Reset = function() {
            this.stack = Array(16);
            this.active = -1;
        };
        for(var i = 0; i < 16; i++) {
            this.stack[i] = 0;
        }
        this.getChar = function(n) {
            return getC(this.stack[n]);
        };
        this.isVisible = function(i) {
            return this.active >= i;
        };
        this.pop = function() {//false bei underflow, ansonsten zahl
            if(this.active < 0) {
                return false;
            }
            else {
                this.isVisible[this.active] = false;
                this.active -= 1;
                var cur = this.stack[this.active+1];
                this.stack[this.active+1] = 0;
                return cur;
            }
        };
        this.push = function(n) {//false bei overflow, ansonsten true
            if(this.active >= 15) {
                return false;
            }
            else {
                this.active++;
                this.stack[this.active] = n;
                this.isVisible[this.active] = true;
                return true;
            }
        };
    });
    
    var userInputIsActive = false;
    var globalX = 0;
    var globalY = 0;

    app.service("Interpreter", ["Stack","Tape","Memory", "MyConsole", function(Stack, Tape, Memory, MyConsole) {
        this.edX = 1;
        this.edY = 0;
        this.modusQ = false;
        this.exeFromTape = false;
        this.running = true;

        this.loopRefStack = Array(0);

        this.Init = function() {
            this.edX = 1;
            this.edY = 0;
            this.modusQ = false;
            this.running = true;
            this.exeFromTape = false;
            userInputIsActive = false;
            Stack.Reset();
            Tape.Reset();
            Memory.Reset();
        };
        this.Reset = function() {
            this.edX = 1;
            this.edY = 0;
            this.modusQ = false;
            this.running = true;
            this.exeFromTape = false;
            userInputIsActive = false;
            Stack.Reset();
            Tape.Reset();
            Memory.setReference(0,0);
        }
        this.jumpMemRef = function(command) {
            if(command == 60) {this.edX = -1; this.edY = 0;}
            if(command == 62) {this.edX = 1; this.edY = 0;}
            if(command == 94) {this.edX = 0; this.edY = -1;}
            if(command == 118){this.edX = 0; this.edY = 1;}
            var fromStack = 1;
            if(this.modusQ) {fromStack = Stack.pop(); this.modusQ = false;}
            if(fromStack === false) {
                MyConsole.debugOut("W", "Warning: stack is already empty. pop() for Instruction Pointer returned 0 at "+Memory.x+"|"+Memory.y);
                fromStack=0;
            }
            Memory.addToReference(this.edX*fromStack, this.edY*fromStack);
        };
        this.changeTapeValue = function(command) {
            var vz = 1;
            if(command == 45) {vz =-1;}
            var fromStack = 1;
            if(this.modusQ) {fromStack = Stack.pop(); this.modusQ = false;}
            if(fromStack === false) {
                MyConsole.debugOut("W", "Warning: stack is already empty. pop() to tape returned 0 at "+Memory.x+"|"+Memory.y);
                fromStack=0;
            }
            Tape.addToValue(vz*fromStack);
            Memory.addToReference(this.edX, this.edY);
        };
        this.changeTapeRef = function(command) {
            var vz = 1;
            if(command == 123) {vz =-1;}
            var fromStack = 1;
            if(this.modusQ) {fromStack = Stack.pop(); this.modusQ = false;}
            if(fromStack === false) {
                MyConsole.debugOut("W", "Warning: stack is already empty. pop() for moving the tape R/W-Head returned 0 at "+Memory.x+"|"+Memory.y);
                fromStack=0;
            }
            Tape.addToReference(vz*fromStack);
            Memory.addToReference(this.edX, this.edY);
        };
        this.LoopEnd = function() {
            var toProof = false;
            if(this.modusQ) {//Stack verwenden
                var fromStack = Stack.pop();
                if(fromStack === false) {
                    MyConsole.debugOut("W", "Warning: stack is already empty. pop() for loop condition returned 0 at "+Memory.x+"|"+Memory.y);
                }
                else if(fromStack != 0) {
                    toProof = true;
                }
                this.modusQ = false;
            }
            else {//Tape verwenden
                var fromTape = Tape.getValue(Tape.active);
                if(fromTape != 0) {toProof = true;}
            }

            var loopBegin = this.loopRefStack.pop();
            if(loopBegin === undefined) {
                MyConsole.debugOut("W", "Warning: no refering loop begin for the loop end at "+Memory.x+"|"+Memory.y);
                Memory.addToReference(this.edX, this.edY);
            }
            else if(toProof) {
                Memory.setReference(loopBegin[0], loopBegin[1]);
            }
            else {
                Memory.addToReference(this.edX, this.edY);
            }
        };
        this.Step = function() {
            //Load current command
            var toExe = Memory.getValue();
            if(this.exeFromTape === true) {
                toExe = Tape.getValue(Tape.active);
                this.exeFromTape = false;
                if(toExe == 33) {
                    alert("ERROR");
                    MyConsole.debugOut("E", "Error: infinite loop between tape and memory at "+Memory.x+"|"+Memory.y);
                    MyConsole.debugOut("E", "Error: programm exited at "+Memory.x+"|"+Memory.y);
                    this.running = false;
                    toExe = 0;
                }
            }
            if(userInputIsActive) {
                toExe = 404;//Zum Umgehen des Switch-case
            }
            var save1 = 0;
            //execute command
            if(toExe != 0) {
                switch(toExe) {
                case 60: //<
                case 62: //>
                case 94: //^
                case 118: //v
                    this.jumpMemRef(toExe);
                    break;
                case 126: //~ pop()
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by ~ at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    Memory.addToReference(this.edX, this.edY);
                    save1 = Stack.pop();
                    if(!save1) {
                        MyConsole.debugOut("W", "Warning: stack is already empty. pop() to memory returned 0 at "+Memory.x+"|"+Memory.y);
                        save1 = 0;
                    }
                    Memory.setValue(save1);
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 61: //= push(n)
                    if(this.modusQ) {
                        this.modusQ = false;
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by = at "+Memory.x+"|"+Memory.y);
                    }
                    Memory.addToReference(this.edX, this.edY);
                    save1 = Stack.push(Memory.getValue());
                    if(!save1) {
                        MyConsole.debugOut("E", "Error: stackoverflow at "+Memory.x+"|"+Memory.y);
                        MyConsole.debugOut("E", "Error: programm exited at "+Memory.x+"|"+Memory.y);
                        this.running = false;
                    }
                    else {Memory.addToReference(this.edX, this.edY);}

                    break;
                case 63: //? Use Stack
                    this.modusQ = true;
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 43: //+ change tapevalue
                case 45: //- change tapevalue
                    this.changeTapeValue(toExe);
                    break;
                case 123: // { tape-Reference
                case 125: // } tape-Reference
                    this.changeTapeRef(toExe);
                    break;
                case 46: //. Stack -> Tape
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by . at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    save1 = Stack.pop();
                    if(save1 === false){
                        MyConsole.debugOut("W", "Warning: stack is already empty. pop() to memory returned 0 at "+Memory.x+"|"+Memory.y);
                        save1=0;
                    }
                    Tape.setValue(save1);
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 44: //, Memory -> Tape
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by , at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    Memory.addToReference(this.edX, this.edY);
                    Tape.setValue(Memory.getValue());
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 35: //# Tape -> Memory
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by # at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    Memory.addToReference(this.edX, this.edY);
                    Memory.setValue(Tape.getValue(Tape.active));
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 33: //! Tape as command
                    this.exeFromTape = true;
                    this.Step();
                    break;
                case 59: //; Userinput
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by ; at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    userInputIsActive = true;
                    globalX = this.edX;
                    globalY = this.edY;
                    MyConsole.debugOut("M", "Message: waiting for userinput")
                    break;
                case 34: //" Console-Output
                    save1 = '';
                    if(this.modusQ) {
                        save1=Stack.pop();
                        if(save1 === false) {
                            MyConsole.debugOut("W", "Warning: Stack is already empty. pop() for console output returned 0 at "+Memory.x+"|"+Memory.y);
                            save1 = 0;
                        }
                        save1 = getC(save1);
                        this.modusQ = false;
                    }
                    else {
                        Memory.addToReference(this.edX, this.edY);
                        save1 = getC(Memory.getValue());
                    }
                    MyConsole.consoleOut(save1);
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 91: //[ Loop-begin
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by [ at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    this.loopRefStack.push([Memory.x,Memory.y]);
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 93: //] Loop-end
                    this.LoopEnd();
                    break;
                case 42: //* NOP
                    if(this.modusQ) {
                        MyConsole.debugOut("W", "Warning: 'use stack' was ignored and discarded by * at "+Memory.x+"|"+Memory.y);
                        this.modusQ = false;
                    }
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 47: // / if-Statement
                    save1 = Tape.getValue(Tape.active);
                    if(this.modusQ) {
                        save1 = Stack.pop();
                        if(save1 === false) {
                            MyConsole.debugOut("W", "Warning: Stack is already empty. pop() for console output returned 0 at "+Memory.x+"|"+Memory.y);
                            save1 = 0;
                        }
                        this.modusQ = false;
                    }
                    if(save1 == 0) {
                        //change direction clock-wise
                        save1 = this.edX;
                        this.edX = this.edY;
                        this.edY = save1;
                        if(this.edY == 0) {
                            this.edX *= -1;
                        }
                    }
                    Memory.addToReference(this.edX, this.edY);
                    break;
                case 124: //| Absolute jump
                    var iFirst = 0, iSecond = 0;
                    if(this.modusQ) {
                        //pop 2 elements
                        iFirst = Stack.pop();
                        if(iFirst === false) {
                            MyConsole.debugOut("W", "Warning: Stack is already empty. pop() for console output returned 0 at "+Memory.x+"|"+Memory.y);
                            iFirst = 0;
                        }
                        iSecond = Stack.pop();
                        if(iSecond === false) {
                            MyConsole.debugOut("W", "Warning: Stack is already empty. pop() for console output returned 0 at "+Memory.x+"|"+Memory.y);
                            iSecond = 0;
                        }
                        this.modusQ = false;
                    }
                    else {
                        //get 2 elements from Tape
                        iFirst = Tape.getValue(Tape.active);
                        iSecond = Tape.getValue(Tape.active+1);
                    }
                    //make jump to first%16 | sec%16
                    Memory.setReference(iFirst%16, iSecond%16);
                break;
                case 404://Usefull for 'waiting for userInput'
                    break;
                default: //Unvalid operation
                    MyConsole.debugOut("E", "Error: invalid operation at "+Memory.x+"|"+Memory.y);
                    MyConsole.debugOut("E", "Error: programm exited at "+Memory.x+"|"+Memory.y);
                    this.running = false;
                }
            }
            else {
                MyConsole.debugOut("M", "Message: programm exited at "+Memory.x+"|"+Memory.y);
                this.running = false;
            }
        };
    }]);
})();
(function(){
    var app = angular.module("TuringTape", []);
    
    app.directive("tapeElement", function() {
        return {
          restrict: 'E',
          templateUrl: 'tapeElement.html',
          controller: ["Tape", function(Tape){
                this.arr = {arrCell : Tape.tape};
                this.isActive = function(index) {
                    return Tape.isActive(index);
                };
                this.addToActive = function(n){
                    Tape.addToReference(n);
                };
                this.getC = function(n) {
                    return Tape.getChar(n);
                }
                this.getNum = function(n) {
                    return Tape.tape[n];
                }
          }],
          controllerAs: 'tapeCtrl'
        };
    });
})();
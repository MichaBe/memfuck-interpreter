(function() {
    var app = angular.module("StackSave", []);

    app.directive("stackElement", function(){
        return {
            restrict: 'E',
            templateUrl: 'stackElement.html',
            controller: ['Stack', function(Stack){
                this.stk = {cell: Stack.stack};
                this.isVisible = function(i) {
                    return Stack.isVisible(i);
                };
                this.getC = function(i) {
                    return Stack.getChar(i);
                }
                this.getNum = function(i) {
                    return Stack.stack[i];
                }
            }],
            controllerAs: 'stackCtrl'
        };
    });
})();
# Projekt memFuck-Interpreter (Anfang 2017)

Zwischen dem ersten und dem zweiten Semester lernte ich das Framework AngularJS kennen und arbeitete an einer Webapplikation, um das erlernte umzusetzen. Dabei entstand ein Interpreter für eine Programmiersprache, deren Syntax und funktionsweise ich im Verlauf des Projekts entwarf.
Ein großer Einfluss war hierbei die bekannte Programmiersprache Brainf*ck, die mit einer Syntax von nur 8 Symbolen auskommt.

## Verwendete Technologien

* HTML / JavaScript / CSS
* AngularJS 1.5

## Weiterführende Ressourcen

Liveversion: [MemFuck Live-Interpreter](http://codesketch.de/site/content/MemFuck/)